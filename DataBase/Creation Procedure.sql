Create DataBase rnatraining;
use rnatraining;
delimiter $$

CREATE TABLE `historicocam` (
  `IDTreinamento` int(11) NOT NULL,
  `SensorID` int(11) NOT NULL,
  `Contador` int(11) NOT NULL,
  `TimeMessage` decimal(20,4) NOT NULL,
  `Dif` int(11) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `channels` int(11) NOT NULL,
  `Imagem` longblob NOT NULL,
  PRIMARY KEY (`IDTreinamento`,`SensorID`,`Contador`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci$$

delimiter $$

CREATE TABLE `historicoacel` (
  `IDTreinamento` int(11) NOT NULL,
  `SensorID` int(11) NOT NULL,
  `Contador` int(11) NOT NULL,
  `TimeMessage` decimal(20,4) NOT NULL,
  `Dif` int(11) NOT NULL,
  `AccelX` float NOT NULL,
  `AccelY` float NOT NULL,
  `AccelZ` float NOT NULL,
  PRIMARY KEY (`Contador`,`SensorID`,`IDTreinamento`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci$$
