/*
 * UDPServer.h
 *
 *  Created on: Mar 9, 2013
 *      Author: ronald
 */

#ifndef UDPSERVER_H_
#define UDPSERVER_H_

#include <boost/asio.hpp>
#include <list>
//#include <ctime>
#include "thread.h"
using namespace std;

union DATACONVERTER
{
   char   			ch;
   int    			i;
   long   			l;
   float  			f;
   double 			d;
   struct	{
	   char Byte[8];
   }Bytes;
};

struct S_AccelerometerMessage
{
public:
	int SensorID;
	long unsigned int Counter;
	double TempoMensagem;
	long DifMili;
	float VetorAcell[3];
};

typedef list<S_AccelerometerMessage> lS_AccelerometerMessage;
typedef boost::array<char, 1024> achar1024;
typedef list< char> lchar;

class UDPServer: public Thread {
public:
			UDPServer(lS_AccelerometerMessage *messageBuffer); // Creation Function
			UDPServer(lS_AccelerometerMessage *messageBuffer, bool autoStart);
			UDPServer(lS_AccelerometerMessage *messageBuffer, double referenceTime, bool autoStart); // Creation Function
	bool 	isRunning();
	void 	StartServer();
	void 	StartServer(double referenceTime);
	void 	StopServer();
	virtual	~UDPServer(); // Destructor Function
private:
//vars
	bool 							listening;
	bool 							running;
	bool							isReceivingMessage;
	DATACONVERTER					packConverter;
	S_AccelerometerMessage			messageBuffer;
	double 							possibleTimeMesssage;
	double							refTime;
	lS_AccelerometerMessage	*ListOfMessages;
	boost::asio::io_service 		io_service;
	boost::asio::ip::udp::socket 	*socket;
	achar1024						recv_buf;
	size_t							recv_count;
	lchar							pkgBuffer;
	boost::asio::ip::udp::endpoint 	remote_endpoint;
	boost::system::error_code 		error;
	long unsigned int 				messageCounter;
	static const unsigned int STARTBYTESEQ = 3;
	static const unsigned int PKGSIZE = 20;

//functions
	void 	StartupValues(lS_AccelerometerMessage *messageBuffer, double referenceTime, bool autoStart);
	bool 	CloseConnection();
	void 	err(char *str);
	void 	FillMessageBuffer();
	bool 	OpenConnection();
	void 	ReadDataReceived(boost::array<char, 1024> buf, size_t count);
	void 	ResetMessageBuffer();
	void 	*run();
	double 	Getclock();
	bool 	ValidHead();
};

#endif /* UDPSERVER_H_ */
