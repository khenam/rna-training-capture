/*
 * CalibrateCamera.h
 *
 *  Created on: Mar 29, 2013
 *      Author: ronald
 */

#ifndef CALIBRATECAMERA_H_
#define CALIBRATECAMERA_H_
#include "cv.h"
#include <vector>
#include <string>

using namespace cv;
using namespace std;

class CalibrateCamera {
public:
			CalibrateCamera();

	Mat		applyCalibration(Mat frame);
	void	cancelCalibration();
	Mat		findCalibrationObject(Mat frame);
	bool 	isCalibrated();
	bool 	isCalibrating();
	Mat		getCameraMatrix();
	Mat		getDistortionMatrix();
	Mat		getFrameGray();
	void	loadCalibration(string filepathCameraMat, string filepathDistortionMat);
	void	loadCalibration(Mat cameraMatrix, Mat distortionMatrix);
	void	setChessBoardProperties(int horizontalCorners, int verticalCorners);
	void	startCalibration();
	void	startCalibration(int numberBoardReads);
	void	submitFrame(Mat frame);

	virtual	~CalibrateCamera();
private:
//var
	int								numBoradReads;
	int								numCorners;
	int								horizontalCorners;
	int								verticalCorners;
	Size							boardSize;
	bool							calibrated;
	bool							inCalibrationMode;
	vector< vector<cv::Point3f> >	object_points;
	vector< vector<cv::Point2f> >	image_points;
	vector<cv::Point3f> 			obj;
	vector<cv::Point2f>				corners;
	int								winSizecornerSubPix;
	int 							timesFinded;
	Mat								frame;
	Mat								frameGrey;
	Mat 							cameraMatrix;
	Mat								distCoeffs;
	static const int 				DEFAULTBOARDREADS = 8;

//functions
	void	addBoardRead();
	void	finishCalibration();

};

#endif /* CALIBRATECAMERA_H_ */
