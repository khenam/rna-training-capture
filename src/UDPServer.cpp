/*
 * UDPServer.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: ronald
 */

#include "UDPServer.h"
#include "cv.hpp"
//#include <ctime>
//#include <sys/time.h>
//#include <iostream>
#include <string>
#include <boost/cstdint.hpp>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#define BUFLEN 512
#define PORT 5000

namespace {

const unsigned int LIMITOFBUFFERNOTUSED = 300;

}

using boost::asio::ip::udp;

void UDPServer::StartupValues(lS_AccelerometerMessage *messageBuffer, double referenceTime, bool autoStart)
{
	this->ListOfMessages= messageBuffer;
	this->running = false;
	this->listening = false;
	this->isReceivingMessage = false;
	this->refTime = referenceTime;
	this->messageCounter = 0;
	if (autoStart==true) this->StartServer();
}

UDPServer::UDPServer(lS_AccelerometerMessage *messageBuffer) {
	// TODO Auto-generated constructor stub
	double tb = (double)cv::getTickCount();
	this->StartupValues(messageBuffer,tb, false);
}

UDPServer::UDPServer(lS_AccelerometerMessage *messageBuffer, bool autoStart) {
	// TODO Auto-generated constructor stub
	double tb = (double)cv::getTickCount();
	this->StartupValues(messageBuffer,tb, autoStart);
}

UDPServer::UDPServer(lS_AccelerometerMessage *messageBuffer, double referenceTime, bool autoStart) {
	// TODO Auto-generated constructor stub
	this->StartupValues(messageBuffer,referenceTime, autoStart);
}

bool UDPServer::CloseConnection()
{
	if (this->listening)
	{
		this->socket->close();
		this->listening = false;
	}
	return true;
}

void UDPServer::err(char *str)
{
     perror(str);
     //exit(1);
}

bool UDPServer::isRunning()
{
	return (this->running);
}

bool UDPServer::OpenConnection()
{
	try
	{
		this->socket = new udp::socket(this->io_service, udp::endpoint(udp::v4(), PORT));
		this->running = this->listening = true;
	}
	catch (std::exception& e)
	{
		this->running = this->listening = false;
		std::cerr << e.what() << std::endl;
	}
	return this->listening;
}

void UDPServer::ReadDataReceived(boost::array<char, 1024> buf, size_t count)
{
	for(unsigned int i=0; i < count; i++)
	{
		this->pkgBuffer.push_back(buf[i]);
		if (!this->isReceivingMessage)
		{
			this->ValidHead();
		}

		if (this->pkgBuffer.size()==1)
		{
			this->possibleTimeMesssage = this->Getclock();
		}

		//-------------Encapsula e Disponibiliza no Buffer--------------//
		if (this->pkgBuffer.size()== PKGSIZE)
		{
			this->FillMessageBuffer();

			this->ListOfMessages->push_back(this->messageBuffer);
			if (this->ListOfMessages->size() > LIMITOFBUFFERNOTUSED)
			{
				this->ListOfMessages->pop_front();
			}

			this->ResetMessageBuffer();
		}
	}
}

void *UDPServer::run()
{
	while(this->running)
	{
		try
		{
			//-------------Captura de Pacote--------------//
			this->recv_count=this->socket->receive_from(boost::asio::buffer(this->recv_buf),
					this->remote_endpoint, 0, error);

			if (this->error && this-> error != boost::asio::error::message_size)
				throw boost::system::system_error(this->error);

			//*
			//-------------Trata Byte recebido--------------//
			this->ReadDataReceived(this->recv_buf, this->recv_count);
			/*/
			std::string message = make_daytime_string();

			boost::system::error_code ignored_error;
			this->socket->send_to(boost::asio::buffer(message),
					this->remote_endpoint, 0, ignored_error);
			//*/
		}
		catch (std::exception& e) {
			//std::cerr << e.what() << std::endl;
			this->CloseConnection();
		}
	}

     this->CloseConnection();
     return 0;
}

void UDPServer::StartServer(double referenceTime)
{
	this->refTime = referenceTime;
	this->StartServer();
}

void UDPServer::StartServer()
{
	if (this->OpenConnection())
	{
		Thread::start();
	}
}

void UDPServer::StopServer()
{
	this->running = false;
	this->socket->close();//shutdown(boost::asio::ip::udp::socket::shutdown_receive);
	this->io_service.stop();
}


UDPServer::~UDPServer()
{
	// TODO Auto-generated destructor stub
	this->StopServer();
	delete this->socket;
}

void UDPServer::FillMessageBuffer()
{
	int limite;

	//-----------Sensor ID-----------//
	boost::asio::ip::address remote_ad = this->remote_endpoint.address();
	typedef vector< string > split_vector_type;
	string str1(remote_ad.to_string());
	split_vector_type SplitVec;
	boost::algorithm::split( SplitVec, str1, boost::algorithm::is_any_of("."));
	string str2(SplitVec.back());
	//sscanf(str2.data(),"%d",&messageBuffer.SensorID);
	this->messageBuffer.SensorID = atoi(str2.data());

	//-----------Tempo da Mensagem-----------//
	this->messageBuffer.TempoMensagem = this->possibleTimeMesssage;

	//-----------Contador da Mensagem-----------//
	this->messageBuffer.Counter=this->messageCounter++;

	//-----------Delay-----------//
	packConverter.l=0;
	limite = sizeof(packConverter.l);
	for (int i = 0; i<limite; i++)
	{
		this->packConverter.Bytes.Byte[i] = this->pkgBuffer.front();
		this->pkgBuffer.pop_front();
	}
	this->messageBuffer.DifMili = this->packConverter.l;

	//-----------Valores do Acelerometro (x, y, z)-----------//
	//float AccelBuffer;
	limite = sizeof(float);

	for (int j=0; j<3; j++)
	{
		this->packConverter.i=0;
		for (int i = 0; i<limite; i++)
		{
			this->packConverter.Bytes.Byte[i] = this->pkgBuffer.front();
			this->pkgBuffer.pop_front();
		}
		this->messageBuffer.VetorAcell[j]=packConverter.f;
	}
}

void UDPServer::ResetMessageBuffer()
{
	this->isReceivingMessage = false;
}

double UDPServer::Getclock()
{
	double mtime;
	mtime = ((double)cv::getTickCount()-this->refTime)/cv::getTickFrequency();
	return mtime;
}

bool UDPServer::ValidHead()
{
	this->isReceivingMessage = true;
	return(true);

	while (this->pkgBuffer.size() > STARTBYTESEQ)
	{
		/*if ()//Test to Find Start Byte Sequence
		{
			this->isReceivingMessage = true;
			this->pkgBuffer.pop_front();
			this->pkgBuffer.pop_front();
		}
		//*/
		this->pkgBuffer.pop_front();
	}
}

