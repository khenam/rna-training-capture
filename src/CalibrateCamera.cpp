/*
 * CalibrateCamera.cpp
 *
 *  Created on: Mar 29, 2013
 *      Author: ronald
 */

#include "CalibrateCamera.h"

CalibrateCamera::CalibrateCamera() {
	// TODO Auto-generated constructor stub
	this->numBoradReads = DEFAULTBOARDREADS;
	this->horizontalCorners=9;
	this->verticalCorners=6;
	this->numCorners = this->horizontalCorners*this->verticalCorners;
	this->boardSize = Size(this->horizontalCorners, this->verticalCorners);
	this->calibrated = false;
	this->inCalibrationMode = false;
	this->timesFinded = 0;
	this->cameraMatrix = Mat(3, 3, CV_32FC1);

	winSizecornerSubPix = 11;

}

Mat CalibrateCamera::applyCalibration(Mat frame)
{
	if (this->calibrated)
	{
		Mat undistortedMat;
		undistort(frame, undistortedMat, this->cameraMatrix, this->distCoeffs);
		return undistortedMat;
	}
	return frame;
}

bool CalibrateCamera::isCalibrated()
{
	return this->calibrated;
}

bool CalibrateCamera::isCalibrating()
{
	return this->inCalibrationMode;
}

Mat CalibrateCamera::getCameraMatrix()
{
	return this->cameraMatrix;
}

Mat CalibrateCamera::getDistortionMatrix()
{
	return this->distCoeffs;
}

void CalibrateCamera::loadCalibration(string filepathCameraMat, string filepathDistortionMat)
{
}

void CalibrateCamera::loadCalibration(Mat cameraMatrix, Mat distortionMatrix)
{
	//Todo: Teste if CameraMatrix is realy what we need
	this->cameraMatrix = cameraMatrix;

	//Todo: Teste if distortionMatrix is realy what we need
	this->distCoeffs = distortionMatrix;
}

void CalibrateCamera::setChessBoardProperties(int horizontalCorners, int verticalCorners)
{
	this->horizontalCorners=horizontalCorners;
	this->verticalCorners=verticalCorners;
	this->numCorners = this->horizontalCorners*this->verticalCorners;
	this->boardSize = Size(this->horizontalCorners, this->verticalCorners);
}

void CalibrateCamera::startCalibration()
{
	this->startCalibration(DEFAULTBOARDREADS);
}
void CalibrateCamera::startCalibration(int numberBoardReads)
{
	this->calibrated = false;
	this->timesFinded = 0;

	this->cameraMatrix = Mat(3,3,CV_32FC1);
	this->image_points.clear();
	this->object_points.clear();
	this->obj.clear();
	for(int j=0;j<this->numCorners;j++)
		this->obj.push_back(Point3f(j/this->horizontalCorners, j%this->horizontalCorners, 0.0f));

	this->inCalibrationMode = true;
}



void CalibrateCamera::submitFrame(Mat frame)
{
	if (this->inCalibrationMode)
	{
		this->frame = frame;
		bool found = false;
		cvtColor(this->frame, this->frameGrey, CV_BGR2GRAY);

		found = findChessboardCorners(this->frame, this->boardSize, this->corners, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);

		if(found)
		{
			cornerSubPix(this->frameGrey, this->corners, Size(winSizecornerSubPix, winSizecornerSubPix), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
			//drawChessboardCorners(this->frameGrey, this->boardSize, this->corners, found);
			this->addBoardRead();
		}
	}
}

Mat CalibrateCamera::getFrameGray()
{
	return this->frameGrey;
}

CalibrateCamera::~CalibrateCamera() {
	// TODO Auto-generated destructor stub
}

void CalibrateCamera::addBoardRead()
{
	this->image_points.push_back(this->corners);
	this->object_points.push_back(this->obj);
	timesFinded++;

	if(timesFinded>numBoradReads)
	{
		this->finishCalibration();
	}
}

Mat CalibrateCamera::findCalibrationObject(Mat frame)
{
	if (this->inCalibrationMode)
	{
		this->frame = frame;
		bool found = false;
		cvtColor(this->frame, this->frameGrey, CV_BGR2GRAY);

		found = findChessboardCorners(this->frame, this->boardSize, this->corners, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);

		if(found)
		{
			cornerSubPix(this->frameGrey, this->corners, Size(winSizecornerSubPix, winSizecornerSubPix), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
			drawChessboardCorners(this->frameGrey, this->boardSize, this->corners, found);
		}
		return this->frameGrey;
	}
	return frame;
}

void CalibrateCamera::cancelCalibration()
{
	this->calibrated = false;
	this->inCalibrationMode = false;
}

void CalibrateCamera::finishCalibration()
{
	vector<Mat> rvecs;
	vector<Mat> tvecs;

	this->cameraMatrix.ptr<float>(0)[0]=1;
	this->cameraMatrix.ptr<float>(1)[1]=1;

	calibrateCamera(this->object_points, this->image_points, this->frame.size(), this->cameraMatrix, this->distCoeffs, rvecs, tvecs);

	this->calibrated = true;
	this->inCalibrationMode = false;
}
