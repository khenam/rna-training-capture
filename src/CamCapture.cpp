/*
 * CamCapture.cpp
 *
 *  Created on: Mar 5, 2013
 *      Author: ronald
 */

#include "CamCapture.h"
#include <cstdio>

bool CamCapture::fillList = false;

void CamCapture::StartupValues(int device,lS_FrameMessage *frameBuffer, double referenceTime, bool autoStart)
{
	this->camID = device;
	this->capture = new cv::VideoCapture(this->camID);
	this->capture->set(CV_CAP_PROP_FPS,120.0f);
	this->ListOfFrames = frameBuffer;
	this->inCapture = false;
	this->loadImage = false;
	//CamCapture::fillList = false;
	this->frameCounter = 0;
	this->t0 = referenceTime;
	if (autoStart == true) this->StartCapture();
}

CamCapture::CamCapture(int device, lS_FrameMessage *frameBuffer) {
	// TODO Auto-generated constructor stub
	double tb = (double)cv::getTickCount();
	this->StartupValues(device,frameBuffer,tb, false);
}

CamCapture::CamCapture(int device, lS_FrameMessage *frameBuffer, bool autoStart) {
	// TODO Auto-generated constructor stub
	double tb = (double)cv::getTickCount();
	this->StartupValues(device,frameBuffer,tb, autoStart);
}

CamCapture::CamCapture(lS_FrameMessage *frameBuffer) {
	// TODO Auto-generated constructor stub
	double tb = (double)cv::getTickCount();
	this->StartupValues(0,frameBuffer,tb, false);
}

CamCapture::CamCapture(lS_FrameMessage *frameBuffer, bool autoStart) {
	// TODO Auto-generated constructor stub
	double tb = (double)cv::getTickCount();
	this->StartupValues(0,frameBuffer,tb, autoStart);
}

CamCapture::CamCapture(lS_FrameMessage *frameBuffer, double referenceTime, bool autoStart) {
	// TODO Auto-generated constructor stub
	this->StartupValues(0,frameBuffer,referenceTime, autoStart);
}

CamCapture::CamCapture(int device, lS_FrameMessage *frameBuffer, double referenceTime, bool autoStart) {
	// TODO Auto-generated constructor stub
	this->StartupValues(device,frameBuffer,referenceTime, autoStart);
}
void CamCapture::ExportFrameDbg()
{
	char buffer[20];
	std::sprintf(buffer,"FramesCam%d",this->camID);
	this->nameFrameWindow = buffer;
	/*
	cv::namedWindow(this->nameFrameWindow.data(),CV_WINDOW_AUTOSIZE);
	//*/
	this->showFrames = true;
}

void CamCapture::StopExportFrameDbg()
{
	this->showFrames = false;
}

int CamCapture::getDeviceID()
{
	return camID;
}

double CamCapture::Showclock(double tb)
{
	double mtime;
	mtime = (tb - this->t0)/cv::getTickFrequency();
	return mtime;
}

long CamCapture::diffclock()
{
	long diffms;

	diffms = (this->t2 - this->t1)/cv::getTickFrequency()*1000;

    this->t1=this->t2;
    return diffms;
}

bool CamCapture::OpenCapture()
{
	this->capture->set(CV_CAP_PROP_FPS,120);
	this->capture->open(this->camID);
	return(this->capture->isOpened());
}

bool CamCapture::captureIsLoaded(){
	if ( !this->capture->isOpened() ) {
		return(this->OpenCapture());
	}
	return(true);
}

cv::Mat CamCapture::getEdges(){
	return(this->edges);
}

bool CamCapture::isInCapture(){
	return(this->inCapture);
}

void *CamCapture::run()
{
	this->t1 = this->t2=(double)cv::getTickCount();
	while ( this->inCapture ) {
		*(this->capture) >> this->frame; // get a new frame from camera

		this->t2=(double)cv::getTickCount();

		this->frameBuffer.SensorID = this->camID;
		this->frameBuffer.Counter = this->frameCounter++;
		this->frameBuffer.TempoMensagem = this->Showclock(this->t2);
		this->frameBuffer.DifMili = this->diffclock();
		this->frameBuffer.Imagem=this->frame;

		do
		{
			if (CamCapture::fillList == false)
			{
				CamCapture::fillList = true;
				this->ListOfFrames->push_back(this->frameBuffer);
				if (this->ListOfFrames->size() > LIMITOFBUFFERNOTUSED)
				{
					this->ListOfFrames->pop_front();
				}
				CamCapture::fillList = false;
			}
		}while(CamCapture::fillList == true);

		if (this->showFrames==true)
		{
			this->ListOfFramesDbg.push_back(this->frameBuffer);
			if (this->ListOfFramesDbg.size()>LIMITOFBUFFERNOTUSED/3)
			{
				this->ListOfFramesDbg.pop_front();
			}
		}
	}
	this->FinishCapture();
	return 0;
}
void CamCapture::FinishCapture()
{
	//this->capture->release();
}
void CamCapture::StartCapture(double referenceTime)
{
	this->t0 = referenceTime;
	this->StartCapture();
}

void CamCapture::StartCapture(){
	// Inicia o processo de Captura de Imagem
	this->inCapture = this->captureIsLoaded();
	if (this->inCapture)
		Thread::start();
	else
		camID = -1;
}

void CamCapture::StopCapture(){
	// Finaliza o processo de Captura de Imagem
	this->inCapture = false;
}

CamCapture::~CamCapture() {
	// Auto-generated destructor stub
	delete this->capture;
}
