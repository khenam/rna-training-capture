/*
 * EffectOCV.h
 *
 *  Created on: Mar 24, 2013
 *      Author: ronald
 */

#ifndef EFFECTOCV_H_
#define EFFECTOCV_H_

#include "CalibrateCamera.h"
#include "opencv.hpp"
#include <vector>

const int HORIZONTALFLIP_OCV = 1;

const int NOEFFECT = 0;
const int FLIPIMAGEHORIZ = 1;
const int AFFINTRANSFOMRATION = 2;
const int METRICRECOVER = 3;
const int BORDERDETECTION = 4;

struct S_Mat_Corners
{
	cv::Mat Matrix;
	std::vector<cv::Point2f> internalCorners;
	cv::Size boardSize;
	bool found;
};

class EffectOCV {
public:
//Vars


//Functions
	EffectOCV(CalibrateCamera* calibreCam);
	cv::Mat ApplyEffect(cv::Mat ImageBuffer,int EffectIndex);
	cv::Mat	AffineTransform(cv::Mat ImageBuffer);
	cv::Mat	flipImage(const cv::Mat& ImageBuffer, int flipType);
	cv::Mat MetricRecover(cv::Mat ImageBuffer);
	cv::Mat BorderDetection(cv::Mat ImageBuffer);
	static S_Mat_Corners	findChessBoardObject(cv::Mat frameBuffer, cv::Size boardSize, int winSizecornerSubPix, bool DrawCorners);
	static S_Mat_Corners	findChessBoardObject(cv::Mat frameBuffer, cv::Size boardSize, int winSizecornerSubPix);
	static S_Mat_Corners	findChessBoardObject(cv::Mat frameBuffer, cv::Size boardSize);
	static S_Mat_Corners	findChessBoardObject(cv::Mat frame);
	void	setLengths(float hLength,float vLength);
	void	setImageDimensions(int width,int height);
	void 	SetDistortionMatrix(std::vector<cv::Point2f> mapedPoints);
	virtual	~EffectOCV();

	double getAngle();
	void setAngle(double angle);
	int getCheightImg();
	void setCheightImg(int cheightImg);
	int getCwidthImg();
	void setCwidthImg(int cwidthImg);
	int getDheightImg();
	void setDheightImg(int dheightImg);
	const std::vector<cv::Point2f>& getDstTri();
	void setDstTri(const std::vector<cv::Point2f>& dstTri);
	int getDwidthImg();
	void setDwidthImg(int dwidthImg);
	int getHeightImg();
	void setHeightImg(int heightImg);
	float getvLength();
	const cv::Mat& getRotMat();
	double getScaleImg();
	void setScaleImg(double scaleImg);
	const std::vector<cv::Point2f>& getSrcTri();
	void setSrcTri(const std::vector<cv::Point2f>& srcTri);
	float gethLength();
	const cv::Mat& getWarpMat();
	int getWidthImg();
	void setWidthImg(int widthImg);
	bool isUsingCalibration();
	void setUsingCalibration(bool usingCalibration);

private:
//Vars
	CalibrateCamera* 			calibreCam;
	std::vector<cv::Point2f>	srcTri;
	std::vector<cv::Point2f>	dstTri;
	int 						widthImg;
	int 						heightImg;
	int 						cwidthImg;
	int 						cheightImg;
	int 						dwidthImg;
	int 						dheightImg;
	double 						angle;
	double 						scaleImg;
	cv::Mat 					warp_mat;
	cv::Mat						rot_mat;
	float 						hLength;
	float						vLength; // target image’s rectangular sizes
	bool						usingCalibration;
//Functions
	void						updateWarpMatrix();
	void						updateRotationMatrix();
	std::vector<cv::Point2f>	CreateDstTri();
};

#endif /* EFFECTOCV_H_ */
