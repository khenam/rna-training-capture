/*
 * CamCapture.h
 *
 *  Created on: Mar 5, 2013
 *      Author: ronald
 */

#ifndef CAMCAPTURE_H_
#define CAMCAPTURE_H_

#include "opencv.hpp"
#include <list>
//#include <ctime>
//#include <sys/time.h>
#include "thread.h"

using namespace std;

struct S_FrameMessage
{
public:
	int SensorID;
	long unsigned int Counter;
	double TempoMensagem;
	long DifMili;
	cv::Mat Imagem;
};

typedef list<S_FrameMessage> lS_FrameMessage;


class CamCapture : public Thread{
public:
//Var
	lS_FrameMessage						ListOfFramesDbg;
	string								nameFrameWindow;
//Functions
			CamCapture(lS_FrameMessage *frameBuffer);
			CamCapture(int device, lS_FrameMessage *frameBuffer);
			CamCapture(lS_FrameMessage *frameBuffer, bool autoStart);
			CamCapture(int device, lS_FrameMessage *frameBuffer, bool autoStart);
			CamCapture(lS_FrameMessage *frameBuffer, double referenceTime, bool autoStart);
			CamCapture(int device, lS_FrameMessage *frameBuffer, double referenceTime, bool autoStart);
	bool	captureIsLoaded();
	void 	StartCapture();
	void 	StartCapture(double referenceTime);
	void 	StopCapture();
	void 	ExportFrameDbg();
	void 	StopExportFrameDbg();
	int		getDeviceID();
	virtual	~CamCapture();
private:
//Var
	lS_FrameMessage*					ListOfFrames;
	cv::VideoCapture* 					capture;
	S_FrameMessage  					frameBuffer;
	long								lastRead;
	cv::Mat 							edges;
	cv::Mat 							frame;
	bool								inCapture;
	bool								loadImage;
	bool								showFrames;
	static bool							fillList;
	//high_resolution_clock::time_point 	t1,t2;
	double								t0,t1,t2;
	int									camID;
	unsigned long int frameCounter;
	static const unsigned int LIMITOFBUFFERNOTUSED = 300;
//Functions
	void	StartupValues(int device, lS_FrameMessage *frameBuffer, double referenceTime, bool autoStart);
	void 	FinishCapture();
	bool 	OpenCapture();
	bool	isInCapture();
	void 	*run();
	cv::Mat		getEdges();
	long 	diffclock();
	double 	Showclock(double tb);
};

#endif /* CAMCAPTURE_H_ */
