/*
 * DBStorageCamSensors.cpp
 *
 *  Created on: Mar 13, 2013
 *      Author: ronald
 */

#include "DBStorageCamSensors.h"
#include <stdio.h>

DBStorageCamSensors::DBStorageCamSensors(lS_FrameMessage *frameBuffer, lS_AccelerometerMessage *messageBuffer, bool autoStart)
{
	// TODO Auto-generated constructor stub
	this->ListOfFrames = frameBuffer;
	this->ListOfMessages = messageBuffer;
	this->server = new string("localhost");
	this->user = new string("root");
	this->password = new string("");
	this->database = new string("rnatraining");
	this->conn=0;
	this->res=0;
	this->stayConnected = false;
	this->connected = false;
	this->contFrame= 0;
	this->countMessage=0;
	if (autoStart==true) this->StartStorage();
}

bool DBStorageCamSensors::CloseConnection()
{
	if (this->connected)
	{
	/* close connection */
		mysql_close(this->conn);
		this->connected = false;
	}
	return true;
}

bool DBStorageCamSensors::isRunning()
{
	return (this->stayConnected);
}

bool DBStorageCamSensors::OpenConnection()
{
	this->connected = this->stayConnected= false;
	try
	{
		if (!this->connected)
		{
			this->conn = mysql_init(NULL);
			/* Connect to database */
			if (mysql_real_connect(this->conn, this->server->data(), this->user->data(), this->password->data(), this->database->data(), 0, NULL, 0))
			{
				this->connected = this->stayConnected = true;
			}
		}
	}
	catch (std::exception& e)
	{
		this->connected = this->stayConnected = false;
		std::cerr << e.what() << std::endl;
	}
	return this->connected;
}

void *DBStorageCamSensors::run()
{
	this->contFrame= 0;
	this->countMessage=0;
	while ( this->stayConnected ) {
		try
		{
			if(this->ListOfFrames->size())
			{
				if (this->SaveFrame(this->ListOfFrames->front())==true)
				{
					this->ListOfFrames->pop_front();
				}
			}
			if(this->ListOfMessages->size())
			{
				this->SaveMassage(this->ListOfMessages->front());
				this->ListOfMessages->pop_front();
			}

			/* output table name */
			/*
			printf("MySQL Tables in mysql database:\n");
			while ((this->row = mysql_fetch_row(this->res)) != NULL)
				printf("%s \n", this->row[0]);
			//*/
		}
		catch (std::exception& e) {
			std::cerr << e.what() << std::endl;
		}
	}
	this->CloseConnection();
	return 0;
}

void DBStorageCamSensors::StartStorage(){
	// TODO Inicia o processo de Armazenamento dos Dados
	if (this->OpenConnection())
	{
		this->SetTrainingID();
		Thread::start();
	}
}

void DBStorageCamSensors::StopStorage(){
	// TODO Finaliza o processo de Captura de Imagem
	this->stayConnected = false;
}

DBStorageCamSensors::~DBStorageCamSensors() {
	// TODO Auto-generated destructor stub
	this->CloseConnection();
	//if (this->conn!=0) delete this->conn;
	if (this->res!=0) delete this->res;
	if (this->server!=0) delete this->server;
	if (this->user!=0) delete this->user;
	if (this->password!=0) delete this->password;
	if (this->database!=0) delete this->database;
}

bool DBStorageCamSensors::SaveMassage(S_AccelerometerMessage MsgBfr)
{
	//string cmdBuffer;
	char buffer[1200];
	long unsigned timeBuffer1, timeBuffer2;
	static const char* command = "INSERT INTO `historicoacel` VALUE ( %d , %d, %ld, %d.%d, %ld, %f, %f, %f);";

	timeBuffer1 = floor(MsgBfr.TempoMensagem);
	timeBuffer2 = floor((MsgBfr.TempoMensagem-timeBuffer1)*1000.0);
	sprintf(buffer,	command,
			this->trainingID,
			MsgBfr.SensorID,
			MsgBfr.Counter,
			timeBuffer1,
			timeBuffer2,//.ToString("yyyyMMddHHmmss.fff"),
			MsgBfr.DifMili,
			MsgBfr.VetorAcell[0],
			MsgBfr.VetorAcell[1],
			MsgBfr.VetorAcell[2]);

	//cmdBuffer = buffer;
	//this->countMessage++;
	if (this->SendSQLCommand(buffer)==true)
	{
		return true;
	}
	return false;
}

bool DBStorageCamSensors::SaveFrame(S_FrameMessage FrmBfr)
{
	char buffer[1200];
	long unsigned len, firstLen, timeBuffer1, timeBuffer2;
	static const char* command = "INSERT INTO `historicocam` VALUE ( %d , %d, %ld, %d.%d, %ld ";

	timeBuffer1 = floor(FrmBfr.TempoMensagem);
	timeBuffer2 = floor((FrmBfr.TempoMensagem-timeBuffer1)*1000.0);
	firstLen = sprintf(buffer,	command,
				this->trainingID,
				FrmBfr.SensorID,
				FrmBfr.Counter,
				timeBuffer1,
				timeBuffer2,//.ToString("yyyyMMddHHmmss.fff"),
				FrmBfr.DifMili);

	len = 0;
	if (!FrmBfr.Imagem.empty())
	{
		const char *stat = "%s, %d, %d, %d, '%s');";
		vector<uchar> buf;
		imencode(".bmp", FrmBfr.Imagem, buf, std::vector<int>() );
		unsigned char *imageBuf = (unsigned char *)new char[buf.size()];
		memcpy(imageBuf, &buf[0], buf.size());
		//long unsigned elementSize =FrmBfr.Imagem.rows * FrmBfr.Imagem.cols * FrmBfr.Imagem.channels(); //FrmBfr.Imagem.elemSize();
		long unsigned elementSize =buf.size();
		long unsigned maximumSize = elementSize*2+1+firstLen+200;//Mais o texto utilizado para o segundo snprintf
		char *query = new char[maximumSize];
		//char *data = (char *) FrmBfr.Imagem.data;
		char *data = (char *) imageBuf;
		char *chunk = new char[elementSize*2+1];

		mysql_real_escape_string(this->conn, chunk, data, elementSize);

		len = snprintf(query, maximumSize, stat,
						buffer,
						FrmBfr.Imagem.cols,
						FrmBfr.Imagem.rows,
						FrmBfr.Imagem.channels(),
						chunk
		);

		this->SendSQLCommand(query, len);

		delete chunk;
		delete query;
		//this->contFrame++;
	}
	else
	{
		return false;
		//this->res=SendSQLCommand(cmdBuffer);
	}

	return true;
}

bool DBStorageCamSensors::SendSQLCommand(string SQLCmd)
{
	if (mysql_query(this->conn, SQLCmd.data()))
	{
		fprintf(stderr, "Error: %s\n",
			           mysql_error(this->conn));
		this->connected = false;
		this->OpenConnection();
		return false;
	}
	return true;
}

MYSQL_ROW DBStorageCamSensors::fetch_row()
{
	this->res=mysql_use_result(this->conn);
	return mysql_fetch_row(this->res);
}


bool DBStorageCamSensors::SendSQLCommand(string SQLCmd, long len)
{
	if (mysql_real_query(this->conn, SQLCmd.data(), len))
	{
		fprintf(stderr, "Error: %s\n",
	           mysql_error(this->conn));
		this->connected = false;
		this->OpenConnection();
		return false;
	}
	return true;
}

void DBStorageCamSensors::SetTrainingID()
{
	string cmdBuffer;

	cmdBuffer = "select Max(UltimoIDTreinamento) from( select (COALESCE(max(IDTreinamento),0)+1) UltimoIDTreinamento from `historicoacel` union select (COALESCE(max(IDTreinamento),0)+1) UltimoIDTreinamento from `historicocam`) Buffer;";

	if (this->SendSQLCommand(cmdBuffer)==true)
	{
		this->row = this->fetch_row();
		this->trainingID = atoi(this->row[0]);
		mysql_free_result(this->res);
		this->res = 0;
	}
}



