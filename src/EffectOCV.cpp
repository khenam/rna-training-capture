/*
 * EffectOCV.cpp
 *
 *  Created on: Mar 24, 2013
 *      Author: ronald
 */

#include "EffectOCV.h"

const int DEFAULTWINSIZECORNERSUBPIX = 11;
const int DEFAULTCHESSDIMETION = 4;
const bool DEFAULTDRAWCORNERS = true;


EffectOCV::EffectOCV(CalibrateCamera* calibreCam) {
	// TODO Auto-generated constructor stub

	this->calibreCam = calibreCam;

	this->widthImg=640;
	this->heightImg = 480;
	this->cwidthImg=this->widthImg/2;
	this->cheightImg = this->heightImg/2;
	this->dwidthImg=0;
	this->dheightImg = 0;
	this->angle = 0;
	this->scaleImg = 1.0;
	this->hLength=168;
	this->vLength=168;

	// Inicialize src Reference
	this->srcTri.push_back(cv::Point2f(0.0,				0.0));
	this->srcTri.push_back(cv::Point2f(0.0,				this->vLength));
	this->srcTri.push_back(cv::Point2f(this->hLength,	this->vLength));

	// Inicialize dst Reference
	this->setDstTri(this->CreateDstTri());

	// Inicialize Rotation Mat
	this->updateRotationMatrix();
}

cv::Mat EffectOCV::flipImage(const cv::Mat& ImageBuffer, int flipType)
{
	cv::Mat localMat;
	cv::Mat OriginalImage = ImageBuffer;

	// Do flip in Image
	//
	cv::flip(OriginalImage, localMat, flipType);

	return(localMat);

}

cv::Mat EffectOCV::ApplyEffect(cv::Mat ImageBuffer, int EffectIndex)
{
	cv::Mat SourceMat;
	cv::Mat localMat;

	if (this->usingCalibration)
		SourceMat = this->calibreCam->applyCalibration(ImageBuffer);
	else
		SourceMat = ImageBuffer;

	switch (EffectIndex)
	{
		case AFFINTRANSFOMRATION:
			localMat=this->AffineTransform(SourceMat);
		break;
		case METRICRECOVER:
			localMat=this->MetricRecover(SourceMat);
		break;
		case FLIPIMAGEHORIZ:
			localMat=this->flipImage(SourceMat, HORIZONTALFLIP_OCV);
		break;
		case BORDERDETECTION:
			localMat=this->BorderDetection(SourceMat);
		break;
		default:
			SourceMat.copyTo(localMat);
		break;
	}
	return(localMat);
}

cv::Mat EffectOCV::BorderDetection(cv::Mat ImageBuffer)
{
	cv::Mat localMat;
	cv::Mat OriginalImage = ImageBuffer;

	cvtColor(OriginalImage, localMat, CV_BGR2GRAY);
	GaussianBlur(localMat, localMat, cv::Size(7,7), 1.5, 1.5);
	Canny(localMat, localMat, 0, 30, 3);

	return(localMat);
}

cv::Mat EffectOCV::AffineTransform(cv::Mat ImageBuffer)
{
	cv::Mat localMat;
	cv::Mat OriginalImage = ImageBuffer;

	// Do the warp transformation
	//
	cv::warpAffine(OriginalImage, localMat, this->warp_mat,localMat.size(),cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0));
	localMat.copyTo(OriginalImage);

	// Do the Rotate transformation
	//
	cv::warpAffine(OriginalImage, localMat, this->rot_mat, localMat.size(),cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0));

	return(localMat);
}

cv::Mat EffectOCV::MetricRecover(cv::Mat ImageBuffer)
{
	cv::Mat localMat;
	cv::Mat OriginalImage = ImageBuffer;

	// Do the warp transformation
	//

	//cv::warpAffine(OriginalImage, localMat, warp_mat,localMat.size(),cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0));
	//localMat.copyTo(OriginalImage);

	OriginalImage.copyTo(localMat);

	// Do the Rotate transformation
	//

	//cv::warpAffine(OriginalImage, localMat, rot_mat, localMat.size(),cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0));

	return(localMat);
}

void EffectOCV::SetDistortionMatrix(std::vector<cv::Point2f> mapedPoints)
{
	this->srcTri = mapedPoints;

	this->dwidthImg=this->srcTri.front().x;
	this->dheightImg=this->srcTri.front().y;
	this->setDstTri(this->CreateDstTri());
}

EffectOCV::~EffectOCV() {
	// TODO Auto-generated destructor stub
}

void EffectOCV::updateWarpMatrix()
{
	this->warp_mat = cv::getAffineTransform( this->srcTri, this->dstTri);
}

void EffectOCV::updateRotationMatrix()
{
	// Compute rotation matrix
	//
	cv::Point2f center = cv::Point2f(
	this->cwidthImg,
	this->cheightImg
	);

	this->rot_mat = cv::getRotationMatrix2D(center, this->angle, this->scaleImg);
}

S_Mat_Corners EffectOCV::findChessBoardObject(cv::Mat frameBuffer, cv::Size boardSize, int winSizecornerSubPix, bool DrawCorners)
{
	S_Mat_Corners structBuffer;

	cv::Mat frame = frameBuffer;
	structBuffer.boardSize = boardSize;

	structBuffer.found = false;

	cv::cvtColor(frame, structBuffer.Matrix, CV_BGR2GRAY);

	structBuffer.found = cv::findChessboardCorners(frame, structBuffer.boardSize, structBuffer.internalCorners, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);

	if(structBuffer.found)
	{
		cv::cornerSubPix(structBuffer.Matrix, structBuffer.internalCorners, Size(winSizecornerSubPix, winSizecornerSubPix), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
		if (DrawCorners)
			cv::drawChessboardCorners(structBuffer.Matrix, structBuffer.boardSize, structBuffer.internalCorners, structBuffer.found);
	}
	return structBuffer;
}

S_Mat_Corners EffectOCV::findChessBoardObject(cv::Mat frameBuffer, cv::Size boardSize, int winSizecornerSubPix)
{
	return EffectOCV::findChessBoardObject(frameBuffer, boardSize, winSizecornerSubPix, DEFAULTDRAWCORNERS);
}

S_Mat_Corners EffectOCV::findChessBoardObject(cv::Mat frameBuffer, cv::Size boardSize)
{
	return EffectOCV::findChessBoardObject(frameBuffer, boardSize, DEFAULTWINSIZECORNERSUBPIX, DEFAULTDRAWCORNERS);
}

S_Mat_Corners EffectOCV::findChessBoardObject(Mat frameBuffer)
{
	cv::Size sizeBuffer=Size(DEFAULTCHESSDIMETION, DEFAULTCHESSDIMETION);
	return EffectOCV::findChessBoardObject(frameBuffer, sizeBuffer, DEFAULTWINSIZECORNERSUBPIX, DEFAULTDRAWCORNERS);
}

std::vector<cv::Point2f> EffectOCV::CreateDstTri()
{
	std::vector<cv::Point2f> dstTriBuffer;
	float outX1, outX2, outX3;//, outX4;
	float outY1, outY2, outY3;//, outY4;

	outX1 = this->dwidthImg + 0;				outY1 = this->dheightImg + 0;
	outX2 = this->dwidthImg + 0;				outY2 = this->dheightImg + this->vLength;
	outX3 = this->dwidthImg + this->hLength;	outY3 = this->dheightImg + this->vLength;

	dstTriBuffer.push_back(cv::Point2f((float) (outX1), (float) (outY1)));
	dstTriBuffer.push_back(cv::Point2f((float) (outX2), (float) (outY2)));
	dstTriBuffer.push_back(cv::Point2f((float) (outX3), (float) (outY3)));

	return dstTriBuffer;
}

//--------Length---------//
float EffectOCV::gethLength()
{
	return this->hLength;
}

float EffectOCV::getvLength()
{
	return this->vLength;
}

void	EffectOCV::setLengths(float hLength,float vLength)
{
	this->hLength = hLength;
	this->vLength = vLength;

	this->setDstTri(this->CreateDstTri());
}

//---------------Dimensions--------------//
void EffectOCV::setImageDimensions(int width, int height)
{
	this->setWidthImg(width);
	this->setHeightImg(height);
}

//--------Height of Image---------//
int EffectOCV::getCheightImg()
{
	return this->cheightImg;
}

void EffectOCV::setCheightImg(int cheightImg)
{
	this->cheightImg = cheightImg;
	this->updateRotationMatrix();
}

int EffectOCV::getDheightImg()
{
	return this->dheightImg;
}

void EffectOCV::setDheightImg(int dheightImg)
{
	this->dheightImg = dheightImg;
	this->setDstTri(this->CreateDstTri());
}

int EffectOCV::getHeightImg()
{
	return this->heightImg;
}

void EffectOCV::setHeightImg(int heightImg)
{
	this->heightImg = heightImg;
	this->setCheightImg(this->heightImg/2);
}

//--------Width of Image---------//
int EffectOCV::getCwidthImg()
{
	return this->cwidthImg;
}

void EffectOCV::setCwidthImg(int cwidthImg)
{
	this->cwidthImg = cwidthImg;
	this->updateRotationMatrix();
}

int EffectOCV::getDwidthImg()
{
	return this->dwidthImg;
}

void EffectOCV::setDwidthImg(int dwidthImg)
{
	this->dwidthImg = dwidthImg;
	this->setDstTri(this->CreateDstTri());
}

int EffectOCV::getWidthImg() {
	return this->widthImg;
}

void EffectOCV::setWidthImg(int widthImg)
{
	this->widthImg = widthImg;
	this->setCwidthImg(this->widthImg/2);
}

//--------Scale---------//
double EffectOCV::getScaleImg()
{
	return this->scaleImg;
}

void EffectOCV::setScaleImg(double scaleImg)
{
	this->scaleImg = scaleImg;
	this->updateRotationMatrix();
}

//--------Angle---------//
double EffectOCV::getAngle()
{
	return this->angle;
}

void EffectOCV::setAngle(double angle)
{
	this->angle = angle;
	this->updateRotationMatrix();
}

//--------Transformation Matrix---------//
const std::vector<cv::Point2f>& EffectOCV::getSrcTri()
{
	return this->srcTri;
}

void EffectOCV::setSrcTri(const std::vector<cv::Point2f>& srcTri)
{
	this->srcTri = srcTri;
/*
	this->dwidthImg=this->srcTri.front().x;
	this->dheightImg=this->srcTri.front().y;
	this->setDstTri(this->CreateDstTri());
*/
	this->updateWarpMatrix();
}

const std::vector<cv::Point2f>& EffectOCV::getDstTri()
{
	return this->dstTri;
}

void EffectOCV::setDstTri(const std::vector<cv::Point2f>& dstTri)
{
	this->dstTri = dstTri;
	this->updateWarpMatrix();
}

const cv::Mat& EffectOCV::getWarpMat()
{
	return this->warp_mat;
}

const cv::Mat& EffectOCV::getRotMat()
{
	return this->rot_mat;
}

//-------- Other Options ---------//
bool EffectOCV::isUsingCalibration()
{
	return this->usingCalibration;
}

void EffectOCV::setUsingCalibration(bool usingCalibration)
{
	this->usingCalibration = usingCalibration;
}

