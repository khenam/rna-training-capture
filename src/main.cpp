#include "highgui.h"
#include "CamCapture.h"
#include "UDPServer.h"
#include "DBStorageCamSensors.h"
#include "EffectOCV.h"
#include <vector>
#include <stdio.h>

const int FIRSTCAMERAID = 0;
const int SECONDCAMERAID = 1;

lS_FrameMessage *frameBuffer;
lS_AccelerometerMessage *messageBuffer;
CamCapture* capture[2];
UDPServer* server;
DBStorageCamSensors* storage;
EffectOCV* effect;
CalibrateCamera* calibreCam;
S_Mat_Corners RegionRef[2];
cv::Mat roi[2];
double refTime;
bool isCalibrating = false;
bool isRunning = false;
bool doubleMode = false;
bool isViewFames = false;
bool storeInDatabase = false;
string ScreenFrameName[2];
int effectImg=NOEFFECT;
int widthImg=640, heightImg = 480;
int cwidthImg=widthImg/2, cheightImg = heightImg/2;
int dwidthImg=0, dheightImg = 0;
std::vector<cv::Point2f> gsrcTri;
double angle = 0;
double scaleImg = 1.0;
cv::Mat warp_mat, rot_mat;
float hLength=168,vLength=168; // target image’s rectangular sizes


void StartCapture(int DeviceID)
{
	isRunning=true;
	cout << "Starting . . ."<<"\n";

	frameBuffer = new lS_FrameMessage;
	messageBuffer = new lS_AccelerometerMessage;

	refTime = (double)cv::getTickCount();
	capture[FIRSTCAMERAID] = new CamCapture(DeviceID,frameBuffer, refTime, true);
	if (!capture[FIRSTCAMERAID]->captureIsLoaded())
	{
		isRunning=false;
		cout << "Aborting . . ."<<"\n";
		cout << "Problems to Start Device! Please Check the Equipment or Device ID"<<"\n";
	}
	else
	{
		if (!isCalibrating)
		{
			server = new UDPServer(messageBuffer, refTime, true);
			if (storeInDatabase==true)
			{
				storage= new DBStorageCamSensors(frameBuffer, messageBuffer, true);
			}
		}
	}
}

void SelectDevice()
{
	char ch;
	do
	{
		cout << "Please Choose a Device ID: ";
		cin >> ch;
		if (ch>='0' && ch<='9')
		{
			int DeviceID = ch-char('0');
			StartCapture(DeviceID);
		}
		else
		{
			cout << "Please Choose a Device ID between 0 and 9 !" << "\n\n" << "TryAgain" << "\n\n\n";
		}
	}while(ch<'0' || ch>'9');
}

void StopCapture()
{
	isRunning=false;
	cout << "Stopping . . ."<<"\n";

	capture[FIRSTCAMERAID]->StopCapture();
	if (doubleMode==true)
	{
		capture[SECONDCAMERAID]->StopCapture();
	}
	if (!isCalibrating)
	{
		server->StopServer();
		if (storeInDatabase==true)
		{
			storage->StopStorage();
		}
	}

	sleep(3);

	delete capture[FIRSTCAMERAID];
	if (doubleMode==true)
	{
		delete capture[SECONDCAMERAID];
	}
	if (!isCalibrating)
	{
		delete server;
		if (storeInDatabase==true)
		{
			delete storage;
		}
		delete frameBuffer;
		delete messageBuffer;
	}

	doubleMode=false;
}

void StartCapture()
{
	isRunning=true;
	cout << "Starting . . ."<<"\n";

	frameBuffer = new lS_FrameMessage;
	messageBuffer = new lS_AccelerometerMessage;

	refTime = (double)cv::getTickCount();
	capture[FIRSTCAMERAID] = new CamCapture(FIRSTCAMERAID,frameBuffer, refTime, true);
	if (doubleMode==true)
	{
		capture[SECONDCAMERAID] = new CamCapture(SECONDCAMERAID,frameBuffer, refTime, true);
	}
	if (!capture[FIRSTCAMERAID]->captureIsLoaded() || ((doubleMode==true) && !capture[SECONDCAMERAID]->captureIsLoaded()))
	{
		//isRunning=false;
		StopCapture();
		cout << "Aborting . . ."<<"\n";
		cout << "Problems to Start Device! Please Check the Equipment or Device ID"<<"\n";
	}
	else
	{
		server = new UDPServer(messageBuffer, refTime, true);
		if (storeInDatabase==true)
		{
			storage= new DBStorageCamSensors(frameBuffer, messageBuffer, true);
		}
	}
}

void doCameraCalibration()
{
	string CalibrationWindowName = "Calibration";
	string CameraWindowName = "CameraCapture";
	cv::namedWindow(CameraWindowName.data(),CV_WINDOW_AUTOSIZE);
	cv::namedWindow(CalibrationWindowName.data(),CV_WINDOW_AUTOSIZE);

	while(calibreCam->isCalibrating())
	{
		if (frameBuffer->size())
		{
			cv::Mat	matOriginal = frameBuffer->front().Imagem;
			cv::imshow(CameraWindowName.data(), matOriginal);

			cv::Mat	matReturn = calibreCam->findCalibrationObject(matOriginal);
			cv::imshow(CalibrationWindowName.data(), matReturn);

			frameBuffer->pop_front();

			switch(toupper(cvWaitKey(1) & 255))
			{
				case 27:
					calibreCam->cancelCalibration();
			    break;
				case ' ':
					calibreCam->submitFrame(matOriginal);
				break;
			}
		}
		else
		{
			cvWaitKey(1);
		}
	}

	cv::destroyAllWindows();

	StopCapture();
}

void ConfigScreen()
{
	int		widthImg=640, heightImg = 480;
	float	hLength=168,vLength=168;
	double	scaleImg=1;
	char ch = '\0';
	while(toupper(ch)!='B')
	{
		cout << "Please Choose an Option:\n";
		cout << "\t" << "'d' : Toggle store data in DataBase: Value = "<< storeInDatabase << "\n";
		cout << "\t" << "'i' : Change Size of Image: Value = "<< effect->getWidthImg()<<", "<< effect->getHeightImg() << "\n";
		cout << "\t" << "'s' : Change the Scale of transformations: Value = "<< effect->getScaleImg() << "\n";
		cout << "\t" << "'c' : Calibrate Camera"<< "\n";
		cout << "\t" << "'l' : Change length of reference Object in Image: Value = "<< effect->gethLength()<<", "<< effect->getvLength() << "\n";
		cout << "\t" << "'b' : Go Back to Menu."<< "\n";
		cout << "Option: ";
		cin >> ch;
		switch(toupper(ch))
		{
		   case 'D':
			   storeInDatabase = !storeInDatabase;
		   break;
		   case 'I':
			   cout<<"\n Type the Width and height of the image (width, height): ";
			   cout<<"\n width: ";
			   scanf("%d", &widthImg);
			   cout<<"\n height: ";
			   scanf("%d", &heightImg);
			   effect->setImageDimensions(widthImg,heightImg);
		   break;
		   case 'S':
			   cout<<"\n Type the scale of the image: ";
			   scanf("%lf", &scaleImg);
			   effect->setScaleImg(scaleImg);
		   break;
		   case 'C':
			   int boardReads;
			   cout<<"\n Type the Number of Board Reads: ";
			   scanf("%d", &boardReads);
			   calibreCam->startCalibration(boardReads);
			   isCalibrating = true;
			   SelectDevice();
			   doCameraCalibration();
			   isCalibrating = false;
		   break;
		   case 'L':
			   cout<<"\n Type the length of reference object in image (horizontal, Vertical): ";
			   //scanf("%f\,%f", &hLength, &vLength);
			   cout<<"\n horizontal: ";
			   scanf("%f", &hLength);
			   cout<<"\n Vertical: ";
			   scanf("%f", &vLength);
			   effect->setLengths(hLength,vLength);
		   break;
		}
	}
}


void ToggleView()
{
	if (!isViewFames)
	{
		cout << "Use Esc in view(s) to exit . . ."<<"\n";
		capture[FIRSTCAMERAID]->ExportFrameDbg();
		ScreenFrameName[FIRSTCAMERAID] = capture[FIRSTCAMERAID]->nameFrameWindow;
		cv::namedWindow(ScreenFrameName[FIRSTCAMERAID].data(),CV_WINDOW_AUTOSIZE);
		if (doubleMode==true)
		{
			capture[SECONDCAMERAID]->ExportFrameDbg();
			ScreenFrameName[SECONDCAMERAID] = capture[SECONDCAMERAID]->nameFrameWindow;
			cv::namedWindow(ScreenFrameName[SECONDCAMERAID].data(),CV_WINDOW_AUTOSIZE);
		}
	}
	else
	{
		capture[FIRSTCAMERAID]->StopExportFrameDbg();
		if (doubleMode==true)
		{
			capture[SECONDCAMERAID]->StopExportFrameDbg();
		}

		cv::destroyAllWindows();
	}

}

void SetDistortionMatrix()
{
	std::vector<cv::Point2f> srcTri, dstTri;
	int inX1, inX2, inX3;//, inX4;
	int inY1, inY2, inY3;//, inY4;

	// get the distorted image’s 4 point positions
	cout<<"\n Type the 1st point in the distorted image (x1, y1) : ";
	scanf("%d,%d", &inX1, &inY1);
	cout<<"\n Type the 2nd point in the distorted image (x2, y2) : ";
	scanf("%d,%d", &inX2, &inY2);
	cout<<"\n Type the 3rd point in the distorted image (x3, y3) : ";
	scanf("%d,%d", &inX3, &inY3);
	//cout<<"\n Type the 4th point in the distorted image (x4, y4) : ";
	//scanf("%d,%d", &inX4, &inY4);

	srcTri.push_back(cv::Point2f((float)inX1,(float)inY1));
	srcTri.push_back(cv::Point2f((float)inX2,(float)inY2));
	srcTri.push_back(cv::Point2f((float)inX3,(float)inY3));

	effect->SetDistortionMatrix(srcTri);

}

bool findReference(int RefID, Rect_<float> relation)
{
	bool waitFind = true;
	cv::Mat frame;
	string CalibrationWindowName = "Calibration";
	string CameraWindowName = "CameraCapture";
	cv::namedWindow(CameraWindowName.data(),CV_WINDOW_AUTOSIZE);
	cv::namedWindow(CalibrationWindowName.data(),CV_WINDOW_AUTOSIZE);

	while(waitFind)
	{
		if (frameBuffer->size())
		{
			frame = calibreCam->applyCalibration(frameBuffer->front().Imagem);
			cv::Rect rec(
					trunc(relation.x * frame.cols),
					trunc(relation.y * frame.rows),
					trunc(relation.width * frame.cols),
					trunc(relation.height * frame.rows)
			);

			roi[RefID] = frame(rec);

			cv::Mat	matOriginal = roi[RefID];
			cv::imshow(CameraWindowName.data(), matOriginal);

			RegionRef[RefID] = EffectOCV::findChessBoardObject(matOriginal);

			cv::imshow(CalibrationWindowName.data(), RegionRef[RefID].Matrix);

			frameBuffer->pop_front();

			//waitFind=!RegionRef[RefID].found;
		}

		switch((cvWaitKey(1) & 255))
		{
			case 27:
				waitFind = false;
			break;
			case ' ':
				waitFind=!RegionRef[RefID].found;
			break;
		}

	}
	cv::destroyAllWindows();
	frameBuffer->clear();
	return RegionRef[RefID].found;
}

bool findReference1()
{
	Rect_<float> relation(
			0.0/8,
			4.0/6,
			2.0/8,
			2.0/6
	);

	return findReference(0, relation);
}

bool findReference2()
{
	Rect_<float> relation(
			6.0/8,
			0.0/6,
			2.0/8,
			2.0/6
	);

	return findReference(1, relation);
}

bool FindIntersection(std::vector<cv::Point2f> *vobjPts, std::vector<cv::Point2f> *vimgPts, cv::Mat H)
{
	int width[2], height[2];
	int cVar, widthCalc, heightCalc;
	int a1, a2, b1, b2;
	//cv::Mat H( 3, 3, CV_32F);
	cv::Mat H1( 3, 3, CV_32F);
	cv::Mat H2( 3, 3, CV_32F);
	cv::Mat H11( 3, 3, CV_32F);
	cv::Mat H21( 3, 3, CV_32F);
	cv::Mat H3( 3, 3, CV_32F);
	cv::Point2f objPts[4], imgPts[4];
	std::vector<cv::Point2f> vobjPts1,vimgPts1;
	cv::Point2f objPts1[4], imgPts1[4];
	std::vector<cv::Point2f> vobjPts2,vimgPts2;
	cv::Point2f objPts2[4], imgPts2[4];
	cv::Size sizeBuffer;
	cv::Point pointBuffer;

// Load First Region Information
	cVar = 0;
	width[cVar] = RegionRef[cVar].boardSize.width;
	height[cVar] = RegionRef[cVar].boardSize.height;

	objPts1[0].x = 0;				objPts1[0].y = 0;
	objPts1[1].x = width[cVar] - 1;	objPts1[1].y = 0;
	objPts1[2].x = 0;				objPts1[2].y = height[cVar]-1;
	objPts1[3].x = width[cVar]-1; 	objPts1[3].y = height[cVar]-1;
	imgPts1[0] = RegionRef[cVar].internalCorners[0];
	imgPts1[1] = RegionRef[cVar].internalCorners[width[cVar]-1];
	imgPts1[2] = RegionRef[cVar].internalCorners[(height[cVar]-1)*width[cVar]];
	imgPts1[3] = RegionRef[cVar].internalCorners[((height[cVar]-1)*width[cVar]) + width[cVar]-1];

	vobjPts1.push_back(objPts1[0]);
	vobjPts1.push_back(objPts1[1]);
	vobjPts1.push_back(objPts1[2]);
	vobjPts1.push_back(objPts1[3]);

	vimgPts1.push_back(imgPts1[0]);
	vimgPts1.push_back(imgPts1[1]);
	vimgPts1.push_back(imgPts1[2]);
	vimgPts1.push_back(imgPts1[3]);

	H1 = cv::getPerspectiveTransform( vimgPts1, vobjPts1);

	roi[cVar].locateROI(sizeBuffer, pointBuffer);
	imgPts1[0].x += pointBuffer.x;
	imgPts1[0].y += pointBuffer.y;
	imgPts1[1].x += pointBuffer.x;
	imgPts1[1].y += pointBuffer.y;
	imgPts1[2].x += pointBuffer.x;
	imgPts1[2].y += pointBuffer.y;
	imgPts1[3].x += pointBuffer.x;
	imgPts1[3].y += pointBuffer.y;

	vimgPts1.clear();
	vimgPts1.push_back(imgPts1[0]);
	vimgPts1.push_back(imgPts1[1]);
	vimgPts1.push_back(imgPts1[2]);
	vimgPts1.push_back(imgPts1[3]);

	H11 = cv::getPerspectiveTransform( vimgPts1, vobjPts1);
// Load Second Region Information
	cVar = 1;
	width[cVar] = RegionRef[cVar].boardSize.width;
	height[cVar] = RegionRef[cVar].boardSize.height;

	objPts2[0].x = 0;				objPts2[0].y = 0;
	objPts2[1].x = width[cVar] - 1;	objPts2[1].y = 0;
	objPts2[2].x = 0;				objPts2[2].y = height[cVar]-1;
	objPts2[3].x = width[cVar]-1; 	objPts2[3].y = height[cVar]-1;
	imgPts2[0] = RegionRef[cVar].internalCorners[0];
	imgPts2[1] = RegionRef[cVar].internalCorners[width[cVar]-1];
	imgPts2[2] = RegionRef[cVar].internalCorners[(height[cVar]-1)*width[cVar]];
	imgPts2[3] = RegionRef[cVar].internalCorners[((height[cVar]-1)*width[cVar]) + width[cVar]-1];

	vobjPts2.push_back(objPts2[0]);
	vobjPts2.push_back(objPts2[1]);
	vobjPts2.push_back(objPts2[2]);
	vobjPts2.push_back(objPts2[3]);

	vimgPts2.push_back(imgPts2[0]);
	vimgPts2.push_back(imgPts2[1]);
	vimgPts2.push_back(imgPts2[2]);
	vimgPts2.push_back(imgPts2[3]);

	H2 = cv::getPerspectiveTransform( vimgPts2, vobjPts2);

	roi[cVar].locateROI(sizeBuffer, pointBuffer);
	imgPts2[0].x += pointBuffer.x;
	imgPts2[0].y += pointBuffer.y;
	imgPts2[1].x += pointBuffer.x;
	imgPts2[1].y += pointBuffer.y;
	imgPts2[2].x += pointBuffer.x;
	imgPts2[2].y += pointBuffer.y;
	imgPts2[3].x += pointBuffer.x;
	imgPts2[3].y += pointBuffer.y;

	vimgPts2.clear();
	vimgPts2.push_back(imgPts2[0]);
	vimgPts2.push_back(imgPts2[1]);
	vimgPts2.push_back(imgPts2[2]);
	vimgPts2.push_back(imgPts2[3]);

	H21 = cv::getPerspectiveTransform( vimgPts2, vobjPts2);
/*
-------- Vertical Line ---------
		X2 - X1
a1 = ---------------
		Y2 - Y1

				X2 - X1
b1 = X1 -   ---------------Y1
				Y2 - Y1
*/
	a1 =  (imgPts1[2].x - imgPts1[0].x)/(imgPts1[2].y - imgPts1[0].y);
	b1 = imgPts1[0].x - (a1*imgPts1[0].y);


/*
-------- Horizontal Line ---------
		Y2 - Y1
a2 = ---------------
		X2 - X1

				Y2 - Y1
b2 = Y1 -   ---------------X1
				X2 - X1
*/
	a2 =  (imgPts2[1].y - imgPts2[0].y)/(imgPts2[1].x - imgPts2[0].x);
	b2 = imgPts2[0].y - (a2*imgPts2[0].x);

/*
		(b1 + b2.a1)
X =   ----------------
		(1 - a1.a2)

Y = a2.X + b2
*/
	imgPts[0].x = ((b1+(b2*a1))/(1.0-(a1*a2)));
	imgPts[0].y = ((a2*imgPts[0].x) + b2);



	imgPts[1] = imgPts2[1];
	imgPts[2] = imgPts1[2];



	a1 =  (imgPts2[3].x - imgPts2[1].x)/(imgPts2[3].y - imgPts2[1].y);
	b1 = imgPts2[1].x - (a1*imgPts2[1].y);


	a2 =  (imgPts1[3].y - imgPts1[2].y)/(imgPts1[3].x - imgPts1[2].x);
	b2 = imgPts1[2].y - (a2*imgPts1[2].x);


	imgPts[3].x = ((b1+(b2*a1))/(1.0-(a1*a2)));
	imgPts[3].y = ((a2*imgPts[0].x) + b2);

	widthCalc = round((((imgPts[3].x-imgPts[2].x)+(imgPts[1].x-imgPts[0].x))/2)/(
				(
					((((imgPts1[3].x-imgPts1[2].x)/objPts1[1].x)+((imgPts1[1].x-imgPts1[0].x)/objPts1[1].x))/2)+
					((((imgPts2[3].x-imgPts2[2].x)/objPts2[1].x)+((imgPts2[1].x-imgPts2[0].x)/objPts2[1].x))/2)
				)/2
			)
	);
	heightCalc = round((((imgPts[2].y-imgPts[0].y)+(imgPts[3].y-imgPts[1].y))/2)/(
			(
				((((imgPts1[2].y-imgPts1[0].y)/objPts1[2].y)+((imgPts1[3].y-imgPts1[1].y)/objPts1[2].y))/2)+
				((((imgPts2[2].y-imgPts2[0].y)/objPts2[2].y)+((imgPts2[3].y-imgPts2[1].y)/objPts2[2].y))/2)
			)/2
		)
	);

	widthCalc = sizeBuffer.width-1;
	heightCalc = sizeBuffer.height-1;

	objPts[0].x = 0;			objPts[0].y = 0;
	objPts[1].x = widthCalc;	objPts[1].y = 0;
	objPts[2].x = 0;			objPts[2].y = heightCalc;
	objPts[3].x = widthCalc; 	objPts[3].y = heightCalc;

	vobjPts->push_back(objPts[0]);
	vobjPts->push_back(objPts[1]);
	vobjPts->push_back(objPts[2]);
	vobjPts->push_back(objPts[3]);

	vimgPts->push_back(imgPts[0]);
	vimgPts->push_back(imgPts[1]);
	vimgPts->push_back(imgPts[2]);
	vimgPts->push_back(imgPts[3]);

	H3 = cv::getPerspectiveTransform( *vimgPts, *vobjPts);

	//H = (H1*2+H2*2+H11+H21+H3*6)/12;
	//H = (H1*2+H2*2+H3*4)/8;
	H = H3;

//Debug
	cout << objPts1[1].x << '\t';
	cout << objPts1[1].y << '\n';
	cout << imgPts1[0].x << '\t';
	cout << imgPts1[0].y << '\n';
	cout << objPts2[1].x << '\t';
	cout << objPts2[1].y << '\n';
	cout << imgPts2[0].x << '\t';
	cout << imgPts2[0].y << '\n';

	cout << "\n\n\n";

	cout << imgPts1[0].x << '\t';
	cout << imgPts1[0].y << '\n';
	cout << imgPts1[1].x << '\t';
	cout << imgPts1[1].y << '\n';
	cout << imgPts1[2].x << '\t';
	cout << imgPts1[2].y << '\n';
	cout << imgPts1[3].x << '\t';
	cout << imgPts1[3].y << '\n';

	cout << "\n";

	cout << imgPts[0].x << '\t';
	cout << imgPts[0].y << '\n';
	cout << imgPts[1].x << '\t';
	cout << imgPts[1].y << '\n';
	cout << imgPts[2].x << '\t';
	cout << imgPts[2].y << '\n';
	cout << imgPts[3].x << '\t';
	cout << imgPts[3].y << '\n';

	cout << "\n";

	cout << objPts[0].x << '\t';
	cout << objPts[0].y << '\n';
	cout << objPts[1].x << '\t';
	cout << objPts[1].y << '\n';
	cout << objPts[2].x << '\t';
	cout << objPts[2].y << '\n';
	cout << objPts[3].x << '\t';
	cout << objPts[3].y << '\n';

	cout << "\n\n\n";
	cout << "\n\n\n";

	cout << "M1 = "<< endl << " "  << H1 << endl << endl;
	cout << "M2 = "<< endl << " "  << H2 << endl << endl;
	cout << "M3 = "<< endl << " "  << H3 << endl << endl;
	cout << "M = "<< endl << " "  << H << endl << endl;

	return true;
}

void showPerspectiveTransformation(cv::Mat H, std::vector<cv::Point2f> imgPts)
{
	bool waitFind = true;
	cv::Mat returnMat;
	string CalibrationWindowName = "Perspective";
	string CameraWindowName = "CameraCapture";
	cv::namedWindow(CameraWindowName.data(),CV_WINDOW_AUTOSIZE);
	cv::namedWindow(CalibrationWindowName.data(),CV_WINDOW_AUTOSIZE);

	while(waitFind)
	{
		if (frameBuffer->size())
		{
			cv::Mat	matOriginal = frameBuffer->front().Imagem;

			cv::warpPerspective(
			matOriginal
			,returnMat
			,H
			,cv::Size(matOriginal.cols, matOriginal.rows)
			,CV_INTER_LINEAR //| CV_WARP_INVERSE_MAP | CV_WARP_FILL_OUTLIERS
			//,cv::BORDER_CONSTANT
			//,cv::Scalar(0)
			);

			cv::line(matOriginal, imgPts[0], imgPts[1], Scalar( 255, 0, 0 ),2, 8);
			cv::line(matOriginal, imgPts[1], imgPts[3], Scalar( 255, 0, 0 ),2, 8);
			cv::line(matOriginal, imgPts[3], imgPts[2], Scalar( 255, 0, 0 ),2, 8);
			cv::line(matOriginal, imgPts[2], imgPts[0], Scalar( 255, 0, 0 ),2, 8);

			cv::imshow(CameraWindowName.data(), matOriginal);

			cv::imshow(CalibrationWindowName.data(), returnMat);

			frameBuffer->pop_front();
		}
		switch((cvWaitKey(1) & 255))
		{
			case 27:
				waitFind = false;
			break;
		}
	}
	cv::destroyAllWindows();
	frameBuffer->clear();
}
void startTrainingRecorder(bool Debug)
{
	S_Mat_Corners retorno;
	int deviceID=-1;
	int boardReads;
	cout<<"\n Type the Number of Board Reads: ";
	scanf("%d", &boardReads);
	calibreCam->startCalibration(boardReads);
	isCalibrating = true;
	SelectDevice();
	deviceID = capture[FIRSTCAMERAID]->getDeviceID();
	doCameraCalibration();
	StartCapture(deviceID);

	if (deviceID<0)
	{
		StopCapture();
		isCalibrating = false;
		return;
	}

	//Search in Region 1
	if (!findReference1())
	{
		StopCapture();
		isCalibrating = false;
		return;
	}
	//Search in Region 2
	if (!findReference2())
	{
		StopCapture();
		isCalibrating = false;
		return;
	}

//If DEBUG Enable
	if (Debug)
	{
		//Find Intersections Region 1 and 2 Top-Left
		//Find Intersections Region 1 and 2 Bottom-Right
		//cv::Point2f objPts[4], imgPts[4];
		std::vector<cv::Point2f> vobjPts, vimgPts;
		cv::Mat H( 3, 3, CV_32F);
		if (!FindIntersection(&vobjPts, &vimgPts, H))
		{
			StopCapture();
			isCalibrating = false;
			return;
		}

		//Apply WarpPerspective
		showPerspectiveTransformation(H, vimgPts);
	}

	StopCapture();
	isCalibrating = false;
	StartCapture(deviceID);
}

void startTrainingRecorder()
{
	startTrainingRecorder(true);
}

void ShowDbgView()
{
	if (capture[FIRSTCAMERAID]->ListOfFramesDbg.size())
	{
		cv::Mat	matReturn = effect->ApplyEffect(capture[FIRSTCAMERAID]->ListOfFramesDbg.front().Imagem, effectImg);
		cv::imshow(ScreenFrameName[FIRSTCAMERAID].data(), matReturn);
		capture[FIRSTCAMERAID]->ListOfFramesDbg.pop_front();
	}

	if (doubleMode==true)
	{
		if (capture[SECONDCAMERAID]->ListOfFramesDbg.size())
		{
			cv::imshow(ScreenFrameName[SECONDCAMERAID].data(), capture[SECONDCAMERAID]->ListOfFramesDbg.front().Imagem);
			capture[SECONDCAMERAID]->ListOfFramesDbg.pop_front();
		}
	}
}

using namespace cv;
int main()
{
	calibreCam = new CalibrateCamera();
	effect = new EffectOCV(calibreCam);

   char ch;
   bool stayRunning=true;

   //namedWindow("edges",1);

   while ( stayRunning ) {
	   /*
	   if (messageBuffer.size())
	   {
		   printf("ID=%d | T=%li | df=%li | AX=%f, AY=%f, AZ=%f \n",
				   messageBuffer.front().SensorID,
				   messageBuffer.front().TempoMensagem,
				   messageBuffer.front().DifMili,
				   messageBuffer.front().VetorAcell[0],
				   messageBuffer.front().VetorAcell[1],
				   messageBuffer.front().VetorAcell[2] );
		   messageBuffer.pop_front();
	   }
	   if (frameBuffer.size())
	   {
		   imshow("edges", frameBuffer.front());
	   	   frameBuffer.pop_front();
	   }
	   else
	   //*/
	   if (isViewFames)
	   {
		   ShowDbgView();
		   switch(toupper(cvWaitKey(1) & 255))
		   {
		   	   case 27:
		   		   ToggleView();
		   		   isViewFames = false;
			   break;
		   	   case '0':
					effectImg = NOEFFECT;
				   	//incializationActions();
			   break;
		   	   case '1':
					effectImg = FLIPIMAGEHORIZ;
		   	   break;
		   	   case '2':
					effectImg = AFFINTRANSFOMRATION;
			   break;
		   	   case '4':
					effectImg = BORDERDETECTION;
			   break;
			   case 'D':
				   	SetDistortionMatrix();
			   break;
			   case 'U':
					effect->setUsingCalibration(!effect->isUsingCalibration());
			   break;
		   	   case '<':
					effect->setDwidthImg(effect->getDwidthImg()-20);
			   break;
		   	   case '>':
		   		    effect->setDwidthImg(effect->getDwidthImg()+20);
			   break;
		   	   case '{':
					effect->setDheightImg(effect->getDheightImg()-20);
			   break;
			   case '}':
					effect->setDheightImg(effect->getDheightImg()+20);
			   break;
		   	   case '-':
					effect->setScaleImg(effect->getScaleImg()-0.10);
			   break;
		   	   case '+':
		   		    effect->setScaleImg(effect->getScaleImg()+0.10);
			   break;
		   }
	   }
	   else
	   {
		   cout << "Please Choose an Option:\n";
		   cout << "\t" << "'t' : Start Training Process"<< "\n";
		   cout << "\t" << "'p' : Play the Process of server and capture"<< "\n";
		   cout << "\t" << "'d' : Play the Process of server and capture with double Cam"<< "\n";
		   cout << "\t" << "'v' : View frames captured with Cam"<< "\n";
		   cout << "\t" << "'s' : Stop the Process of server and capture"<< "\n";
		   cout << "\t" << "'c' : Configure some parameters to the Process"<< "\n";
		   cout << "\t" << "'q' : Quit of the application"<< "\n";
		   cout << "Option: ";
		   cin >> ch;
		   switch(toupper(ch))
		   {
		   	   case 'T':
				   if (!isRunning)
				   {
					   startTrainingRecorder();
				   }
				   else
					   cout << "Already Running . . ."<<"\n";
			   break;
		   	   case 'P':
				   if (!isRunning)
				   {
					   SelectDevice();
				   }
				   else
					   cout << "Already Running . . ."<<"\n";
			   break;
			   case 'D':
				   if (!isRunning)
				   {
					   doubleMode = true;
					   StartCapture();
				   }
				   else
					   cout << "Already Running . . ."<<"\n";
			   break;
			   case 'V':
				   if (isRunning)
				   {
					   ToggleView();
					   isViewFames = true;

				   }
				   else
					   cout << "Is Not Running . . ."<<"\n";
			   break;
			   case 'S':
				   if (isRunning)
				   {
					   StopCapture();
				   }
				   else
					   cout << "Already Stoped . . ."<<"\n";
			   break;
			   case 'C':
				   //cout<< "Not Work Yet."<<"\n"<<"Sorry!"<<"\n";
				   if (!isRunning)
				   {
					   ConfigScreen();
				   }
				   else
				   {
					   cout <<"\n"<< "Please Stop the Capture to access this!"<<"\n";
					   sleep(3);
				   }
			   break;
			   case 'Q':
				   if (isRunning)
				   {
					   StopCapture();
				   }
				   cout<<"Thanks!"<<"\n";
				   stayRunning = false;
			   break;
			   default:
				   cout<<"This is not an option!"<<"\n\n\n\n";
				   sleep(3);
			   break;
		   }
	   }
   }

   delete effect;
   return 0;
}
