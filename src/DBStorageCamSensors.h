/*
 * DBStorageCamSensors.h
 *
 *  Created on: Mar 13, 2013
 *      Author: ronald
 */

#ifndef DBSTORAGECAMSENSORS_H_
#define DBSTORAGECAMSENSORS_H_

#include "thread.h"
#include "CamCapture.h"
#include "UDPServer.h"
#include <string>
#include <mysql.h>

class DBStorageCamSensors: public Thread {
public:
	DBStorageCamSensors(lS_FrameMessage *frameBuffer, lS_AccelerometerMessage *messageBuffer, bool autoStart);
	void StopStorage();
	void StartStorage();
	virtual ~DBStorageCamSensors();
	bool isRunning();
private:
//Vars
	lS_FrameMessage*			ListOfFrames;
	lS_AccelerometerMessage*	ListOfMessages;
	MYSQL*							conn;
	MYSQL_RES*						res;
	MYSQL_ROW 						row;
	string*							server;
	string*							user;
	string* 						password;
	string* 						database;
	bool 							stayConnected;
	bool 							connected;
	int								trainingID;
	long							contFrame, countMessage;

//Functions
	void*		run();
	bool 		OpenConnection();
	bool 		CloseConnection();
	bool 		SaveMassage(S_AccelerometerMessage MsgBfr);
	bool 		SaveFrame(S_FrameMessage FrameBuffer);
	void 		SetTrainingID();
	bool 		SendSQLCommand(string SQLCmd);
	bool 		SendSQLCommand(string SQLCmd, long len);
	MYSQL_ROW 	fetch_row();
};

#endif /* DBSTORAGECAMSENSORS_H_ */
